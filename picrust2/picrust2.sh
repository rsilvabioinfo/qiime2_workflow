qiime tools import --type SampleData[SequencesWithQuality] --input-path amostra2918-manifest --output-path amostra2918.qza --input-format SingleEndFastqManifestPhred33V2

qiime dada2 denoise-single --i-demultiplexed-seqs amostra2918.qza --p-trim-left 0 --p-trunc-len 0 --o-representative-sequences amostra2918-rep-seqs-dada2.qza --o-table amostra2918-table-dada2.qza --o-denoising-stats amostra2918-stats-dada2.qza
qiime metadata tabulate --m-input-file amostra2918-stats-dada2.qza --o-visualization amostra2918-stats-dada2.qzv


# continue analysis using this FeatureTable
mv amostra2918-rep-seqs-dada2.qza rep-seqs.qza
mv amostra2918-table-dada2.qza table.qza

qiime feature-classifier classify-sklearn \
  --i-classifier gg-13-8-99-515-806-nb-classifier.qza \
  --i-reads rep-seqs.qza \
  --o-classification taxonomy.qza

qiime metadata tabulate \
  --m-input-file taxonomy.qza \
  --o-visualization taxonomy.qzv


qime picrust2 full-pipeline \
   --i-table mammal_biom.qza \
   --i-seq mammal_seqs.qza \
   --output-dir q2-picrust2_output \
   --p-threads 1 \
   --p-hsp-method pic \
   --p-max-nsti 2 \
   --verbose

qime feature-table summarize \
   --i-table q2-picrust2_output/pathway_abundance.qza \
   --o-visualization q2-picrust2_output/pathway_abundance.qzv

qiime diversity core-metrics \
   --i-table q2-picrust2_output/pathway_abundance.qza \
   --p-sampling-depth 1000 \
   --m-metadata-file sample_metadata.tsv \
   --output-dir core_metrics_out \
   --p-n-jobs 1

qiime tools export \
   --input-path q2-picrust2_output/pathway_abundance.qza \
   --output-path exported

biom convert \
   -i exported/feature-table.biom \
   -o exported/feature-table.biom.tsv \
   --to-tsv

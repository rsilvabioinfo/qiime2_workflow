import pandas as pd
import xmltodict
import requests
import sys

# 'exported/feature-table.biom_pathway_name.tsv'

def add_metacyc_info(feature_table, out_file):
    url = 'https://websvc.biocyc.org/getxml?id=META:%s'
    link = 'https://biocyc.org/META/NEW-IMAGE?type=PATHWAY&object=%s'

    df = pd.read_csv(feature_table, sep='\t', skiprows=1)

    pth = []
    links = []

    for i in df.index:
        tmp = url % requests.utils.quote(df.loc[i, '#OTU ID'])
        r = requests.get(tmp)
        d = xmltodict.parse(r.text)
        pth.append(d['ptools-xml']['Pathway']['common-name']['#text'])
        links.append(link % df.loc[i, '#OTU ID'])

    df['Pathway_name'] = pth
    df['Pathway_link'] = links
    df = df[[df.columns[0]]+df.columns[-2:].tolist()+df.columns[1:-2].tolist()]

    df.to_csv(out_file, sep='\t', index=None)

if __name__ == '__main__':
    add_metacyc_info(sys.argv[1], sys.argv[2])

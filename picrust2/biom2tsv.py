import qiime2
import pandas as pd
import sys

# 'picrust2/taxonomy.qza'
# 'picrust2/table.qza'

def taxonomy_qza2tsv(taxonomy_qza, table_qza, out_file):
    artifact = qiime2.Artifact.load(taxonomy_qza)
    tx = artifact.view(pd.DataFrame)

    artifact = qiime2.Artifact.load(table_qza)
    df = artifact.view(pd.DataFrame)

    df = df.T
    pd.merge(tx[['Taxon']], df, right_index=True,
             left_index=True).to_csv(out_file, sep='\t')

if __name__ == '__main__':
    taxonomy_qza2tsv(sys.argv[1], sys.argv[2],
                     sys.argv[3])
